package main

import (
	"net"

	log "github.com/sirupsen/logrus"
	pb "gitlab.com/zenyukgo/mst/alarm"
	"google.golang.org/grpc"
)

const (
	srvPort = ":10000"
)

type alarmServer struct {
	pb.UnimplementedAvgAlarmServiceServer
}

func main() {
	activePort, err := net.Listen("tcp", srvPort)
	if err != nil {
		log.Error("can not start on port ", srvPort)
	}

	log.Info("Average Alarm service is serving on port ", srvPort)

	srv := grpc.NewServer()
	pb.RegisterAvgAlarmServiceServer(srv, &alarmServer{})
	err = srv.Serve(activePort)
	if err != nil {
		log.Error("can not start serving Alarm service")
	}

	activePort.Close()
	srv.Stop()
}
