# MST

Golang Programming Assignment
-----------------------------------------

Deliverables:

Please copy to the Google drive folder a single ZIP file containing:
-  The code project, source code, and any files needed to build and test.
-  A text document containing instructions to build and test it. 
-  A text document describing assumptions and instructions, and the amount of time spent on implementing the code assignment
-  Please do not include any binaries compiled yourself, just the project files and any instructions on how to compile and run it for evaluation. The ability to follow these instructions is part of the evaluation.

Implement a golang gRPC service running on port :10000 and satisfy the following requirements and API:


Requirements:

1. The service calculates a single average value of numbers provided by Push(...);
2. The service provides a streaming RPC GetAvgAlarmStream(...) with parameter 'threshold'. When current average value becomes greater than the threshold, the corresponding clients will receive a message GetAvgAlarmStreamRes with the value of the average aggregate that was used for comparison;
3. If Push causes the average value to exceed float range, the value is rejected, and the originator of the request receives an error;
4. The service supports 0-100 clients invoking Push(...) concurrently;
5. The service supports 0-100 clients streaming GetAvgAlarmStream(...) concurrently;
6. The service doesn't crash on errors, and all the errors are logged to stderr;



API protobuf:

// Push request. Adds a number to aggregate
message PushReq {
    // Number to push
    float num = 1;
}

// Empty message
message Empty {
}

// Creates a stream of notifications when running average exceeds the threshold provided
message GetAvgAlarmStreamReq {
    // When average of pushed numbers exceeds this threshold, a notification will be sent to the response stream
    float threshold = 1;
}

// Notification message sent to the stream when average aggregate is greater than the threshold
message GetAvgAlarmStreamRes {
    // Current average value that was compared to the threshold
    float avg = 1;
}

service AvgAlarmService {
    // Push a number to aggregate
    // Returns an error when average exceeds float range
    rpc Push(PushReq) returns (Empty) {}

    // Gets a stream of notifications when current average exceeds the threshold provided in request
    // Never returns an error
    rpc GetAvgAlarmStream(GetAvgAlarmStreamReq) returns (stream GetAvgAlarmStreamRes) {}
}

